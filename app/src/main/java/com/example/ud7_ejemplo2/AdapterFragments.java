package com.example.ud7_ejemplo2;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentActivity;

import androidx.viewpager2.adapter.FragmentStateAdapter;

public class AdapterFragments extends FragmentStateAdapter {
    private static final int NUM_PAG = 3;

    public AdapterFragments(FragmentActivity fa) {
        super(fa);
    }

    @NonNull
    @Override
    public Fragment createFragment(int position) {
        if (position == 0) {
            return new Fragment1();
        } else if (position == 1) {
            return new Fragment2();
        } else {
            return new Fragment3();
        }
    }

    @Override
    public int getItemCount() {
        return NUM_PAG;
    }
}