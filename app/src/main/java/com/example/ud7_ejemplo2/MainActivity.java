package com.example.ud7_ejemplo2;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import androidx.viewpager2.widget.ViewPager2;

import android.os.Bundle;

import com.google.android.material.tabs.TabLayout;
import com.google.android.material.tabs.TabLayoutMediator;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Buscamos el ViewPager y el TabLayout
        ViewPager2 viewPager = findViewById(R.id.viewpager);
        TabLayout tabLayout = findViewById(R.id.tabs);

        // Creamos y asignamos el adaptador de Fragments
        AdapterFragments adapter = new AdapterFragments(this);

        viewPager.setAdapter(adapter);

        // Conectamos el ViewPager con el TabLayout mediante la clase TabLayoutMediator
        TabLayoutMediator.TabConfigurationStrategy tabConfigurationStrategy = new TabLayoutMediator.TabConfigurationStrategy() {
            @Override
            public void onConfigureTab(@NonNull TabLayout.Tab tab, int position) {
                String titulo = "";

                if (position == 0) {
                     titulo = getString(R.string.fragment1);
                } else if (position == 1) {
                    titulo = getString(R.string.fragment2);
                } else {
                    titulo = getString(R.string.fragment3);
                }

                tab.setText(titulo);
            }
        };
        TabLayoutMediator tabLayoutMediator = new TabLayoutMediator(tabLayout, viewPager, tabConfigurationStrategy);

        tabLayoutMediator.attach();

    }
}